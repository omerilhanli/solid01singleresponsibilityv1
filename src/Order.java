
public class Order {

	private String orderId;

	private String orderName;

	private int amount;

	private int price;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", orderName=" + orderName + ", amount=" + amount + ", price=" + price
				+ "]";
	}
	// Order encapsulated part
	

	// Order price calculate part
	public int calculate() {

		int totalAmount = amount * price;

		return totalAmount;
	}

}
