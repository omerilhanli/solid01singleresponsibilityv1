
public class Program {

	public static void main(String[] args) {

		// Order tüm değerleri hesaplanacak şekilde üretilir.
		Order order = create();

		// Ancak order instance'ımız içinden calculate metodu çağırılır, toplam miktar
		// hesaplanır,
		// ekrana basılır. Dolayısıyla SRP ihlal edilir. Aslında bu işi farklı bir
		// birimin yapması gerekiyor.
		int totalPrice = order.calculate();

		System.out.println(order.getOrderId()+" id\'li, " + order.getOrderName() + " için totalPrice: " 
														  + totalPrice + "$ olarak hesaplanmıştır.");
	}
	/*
	 * Burada şöyle bir durum mevcut; Order instance; DATA tutmalı! HESAP
	 * yapmamalı!
	 * 
	 * 
	 * Ama hesap veya benzer farklı bir şey yaptığında göründüğü gibi normalde data
	 * tuttuğu halde onun dışında farklı bir işte yaptığı için SRP ihlal durumda
	 * olur. Datalar için gelen değişim request'leri normalken, öteki işler için
	 * gelen değişim request'leri class'ın iki farklı sorumluluğu olduğunu gösterir. Öteki
	 * işlerin class'ın asıl işlerini manipüle ihtimalleri de var. Dolayısıyla
	 * içeridekiler birbirlerine tightly coupled olarak bağlanabilirler. Üstelik
	 * şöyle de bir durum var ki, Order class'ı data tutmayı amaç edinerek
	 * oluşturulmuş bir class. Bu amacın dışındaki her işlem, uyumluluğu
	 * azaltıcaktır. İç uyumluluk yani cohesion azalınca, yine SRP ihlal edilmiş
	 * olucaktır.
	 * 
	 * Özetle tek bir amaç etrafında toplanarak yazılan bir birim, SRP'ye uygundur.
	 * 
	 * -Değişim için tek bir sebep (amaca yönelik 'bir sebep', aynı amaç ekseninde
	 * 'herhangi bir sebep') olacaktır.
	 * 
	 * -Birbirleriyle uyumlu olan işler birarada toplanıcağından cohesion max düzeye
	 *  çıkacaktır.
	 * 
	 * -Tüm uyumlu işler birarada olacağından, dışarıya bağımlılık minimum düzeye
	 *  inecek ve coupled durumu tightly'den loosely'ye dönüşecektir.
	 * 
	 * 
	 * Amaç:
	 * 
	 * " COHESION MAX olacak şekilde, -tüm uyumlu işlerin biraraya toplandığı "
	 * 
	 *   ve aynı zamanda
	 * 
	 * " COUPLED MIN olacak şekilde, -minimum düzeyde coupled oluşturan işlerin birlikte kullanıldığı "
	 * 
	 *   bir yapı yazmaktır.
	 */

	private static Order create() {

		Order order = new Order();

		order.setOrderId("Order01");

		order.setOrderName("OrderFirst");

		order.setPrice(12);

		order.setAmount(3);

		return order;
	}

}
/* SRP nedir, nasıl olmalı/olmamalıdır.
 * 
 * Bir birim birden fazla yerden çağırılıyorsa, o birim birden fazla yeri
 * ilgilendiriyor demektir. Dolayısıyla birden fazla yere bağlılık mevcut.
 * Birden fazla yer için en iyi ihtimalle birden fazla mesela 2 ayrı iş yapıyor
 * demektir. Bir birimin iki ayrı işi varsa SRP ihlal edilmiştir. İki ayrı iş =
 * iki ayrı sorumluluk demektir. İki ayrı sorumluluk olan birimin iç uyumluluğu
 * zayıftır. Tek bir amacın dışına çıkılmış, birden fazla amaç, tek bir birime
 * sığdırılmıştır. Bi bakıma mini god class oluşturulmuştur.
 * 
 * Herhangi bir değişim isteği, birimin öteki kısmını etkileme ihtimali
 * doğurmakta, dolayısıyla shotgun effect olasıdır. Birimin herhangi bir kısmı
 * diğerine sıkı sıkıya bağımlı olma eğiliminde olucaktır. Tightly coupled olan
 * bu eğilim, değişim isteklerinden dolayı kırılgan bir yapıya sebep olucaktır.
 * Dolayısıyla sık sık ve birçok yerin değişme ihtimalleri artacak, kararsız bir
 * yapı ortaya çıkacaktır.
 * 
 * SRP için iki önemli kavram vardır bunlardan biri COUPLED, diğeri COHESION.
 * Birim bu iki kavramı ters orantılı olacak şekilde desteklemelidir. COUPLED
 * minimum düzeyde olmalı iken, COHESION max düzeyde olmalıdır.
 * 
 * COHESION MAX, bize tek bir amaç etrafında birim sağlar. Birbiriyle uyumlu işler
 *  
 * COUPLED MIN, bize tek bir amaç için olabildiğince dışarıdan bağımsız birimsel bir yapı sağlar.
 * 
 * 
 * 
 * 
 * Peki SRP nasıl kurulmalıdır. (COHESION)
 * 
 * -Eğer bir class oluştulacaksa, öncelikle class'ı ilgilendiren amaç net olmalı, tüm data ve manipulasyon
 * davranışları tamamen birbiriyle uyumlu olacak şekilde tasarlanmalıdır. Mesela data tutması gereken Order modelimiz,
 * içeride fiyat hesaplayan bir iş tutmamalı, o işi kullanıcıya vermemelidir. O işi kullanıcı ya kendisi yapmalıdır ya da 
 * yaptıracak farklı bir birim oluşturmalı ve objeyi o birime paslayarak sonucu almalıdır. Dolayısıyla Order modeli 
 * yalnızca data tutmak için field ve bu fieldlara erişim metodları olan getter-setter barındırmalıdır. Bunun ötesinde
 * objenin equal, toString, clone, vs... şeklindeki davranışlarını override ederek kullanıcıya sağlayabilir. Çünkü bu işler 
 * tamamen Order modelinin iç işleri olduğundan, uyumluluk devam edecektir.
 * 
 * -Eğer bir metod oluşturulacaksa, yine aynı şekilde metod birimsel düzeyde birbiriyle uyumlu işleri içermeli,
 * o işler dışındaki işleri Extract Method yardımıyla farklı bir metoda ayırarak kendi sorumluluğundan çıkarmalıdır.
 * Tek bir amaç ekseninde iş yapan metod için gelecek değişim istekleri amacı bozmadan kolaylıkla uygulanabilecektir.
 * 
 * 
 * * Son durumda herkes kendi içinde birbiriyle uyumlu işler yaptığında, herhangi birindeki değişim isteği, 
 *   bir diğerini etkilemeyecektir. Dolayısıyla SRP'ye uygun bir yapı kurulmuş olacaktır.
 * 
 * // COUPLED (Açıkalanacak...)
 * 
 */
